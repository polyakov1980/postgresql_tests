#!/bin/bash


cd "work_dir"
export PGPORT=5435
export PGDATA="var-11"


for i in 1 2 3; do 

echo "iteration $i"

for _LOG in stderr:on csvlog:on syslog:on stderr:off; do
for _DUR in -1 0 1000;do
sed -i -E -e "/^logging_collector/s/.*/logging_collector=${_LOG##*:}/" -e "/^log_destination/s/.*/log_destination=${_LOG%%:*}/" -e "/^log_min_duration_statement/s/.*/log_min_duration_statement=${_DUR}/" ${PGDATA}/postgresql.conf 

#echo "restart instance"
pg_ctl stop -D ${PGDATA} -p ${PGPORT} 1>/dev/null 2>&1
postgres -D ${PGDATA} -p ${PGPORT} 1>/dev/null 2>&1 &
sleep 5

echo "
####################
logging_collector ${_LOG##*:}
log_distination is ${_LOG%%:*}
log_min_duration_statement is ${_DUR}
"
rm -f ${PGDATA}/log/*
#echo "drop and create database"
echo "drop database vpolyakov;create database vpolyakov" | psql -d postgres 1>/dev/null 2>&1

#echo "init pgbench"
pgbench --initialize --scale=3000 --quiet 1>/dev/null 2>&1

#echo "reload config"
#echo "select pg_reload_conf()" | psql 1>/dev/null 2>&1
#echo "show logging_collector; show log_destination; show log_min_duration_statement; \l+ vpolyakov" | psql 

#echo "run pgbench 5 minutes and 20 clients"
pgbench -T 300 -c 20 2>/dev/null

done
done
done
